// import React, { Component } from 'react'
// import Slider from 'react-slick'
// import 'slick-carousel/slick/slick.css'
// import 'slick-carousel/slick/slick-theme.css'
// import { useDispatch } from 'react-redux'

// // export class KhoaHocDaGhiDanh extends Component {
// //   render() {
// //     const settings = {
// //       dots: true,
// //       infinite: true,
// //       speed: 500,
// //       slidesToShow: 1,
// //       slidesToScroll: 1,
// //     }
// //     return (
// //       <div>
// //         <h2> Single Item</h2>
// //         <Slider {...settings}>
// //           <div>
// //             <h3>1</h3>
// //           </div>
// //           <div>
// //             <h3>2</h3>
// //           </div>
// //           <div>
// //             <h3>3</h3>
// //           </div>
// //           <div>
// //             <h3>4</h3>
// //           </div>
// //           <div>
// //             <h3>5</h3>
// //           </div>
// //           <div>
// //             <h3>6</h3>
// //           </div>
// //         </Slider>
// //       </div>
// //     )
// //   }
// // }
// const KhoaHocDaGhiDanh = (props) => {
//   const dispatch = useDispatch()
//   const renderCourse = () => {
//     return props.arrFilm.map((item, index) => {
//       return (
//         <div className="mt-2" key={index}>
//           <Film_Flip item={item} />
//         </div>
//       )
//     })
//   }

//   const settings = {
//     className: 'center variable-width ',
//     centerMode: true,
//     infinite: true,
//     centerPadding: '40px',
//     slidesToShow: 2,
//     speed: 400,
//     rows: 3,
//     slidesPerRow: 2,
//     variableWidth: true,
//     nextArrow: <SampleNextArrow />,
//     prevArrow: <SamplePrevArrow />,
//   }
//   return (
//     <div>
//       <Slider {...settings}>{renderCourse()}</Slider>
//     </div>
//   )
// }

// export default KhoaHocDaGhiDanh
