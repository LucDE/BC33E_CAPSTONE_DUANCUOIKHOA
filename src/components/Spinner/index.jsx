import React from 'react'
import { useSelector } from 'react-redux'
import { HashLoader } from 'react-spinners'

export default function Spinner() {
  let isLoading = useSelector((state) => state.spinnerSlice.isLoading)
  return isLoading ? (
    <div className="fixed w-screen h-screen flex justify-center items-center bg-black top-0 left-0 z-50">
      <HashLoader size={150} color="#36d7b7" />
    </div>
  ) : (
    <></>
  )
}
