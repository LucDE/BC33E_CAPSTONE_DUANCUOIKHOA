import CourseCatalog from 'pages/HomePage/CourseCatalog/CourseCatalog'
import React, { useState } from 'react'
import { userInforLocal } from './../../services/local.service'
import { setUserInfor } from 'redux/slice/userSlice'
import { NavLink, useNavigate } from 'react-router-dom'
import { useDispatch } from 'react-redux'

export default function Header() {
  let [user, setUser] = useState(null)
  let dispatch = useDispatch()
  user = userInforLocal.get()
  let navigate = useNavigate()
  let handleLogOut = () => {
    userInforLocal.remove()
    dispatch(setUserInfor(null))
    setTimeout(() => {
      window.location.reload(navigate('/'))
    }, 500)
  }

  let renderContent = () => {
    if (user) {
      return (
        <div className="space-x-4 font-medium">
          <div className="justify-center flex items-center">
            <div className="dropdown items-center text-white text-sm px-2 rounded-md">
              <button className="text-lg text-white px-4 py-1 rounded-md">
                <i className="fa fa-user-astronaut mr-2" />
                {user.hoTen}
              </button>
              <div className="dropdown-content rounded">
                <NavLink to={`/ThongTinTaiKhoan`} className="rounded-md">
                  Thông tin tài khoản
                  <p className="text-gray-400">{user.email}</p>
                </NavLink>
              </div>
            </div>
            <button
              onClick={handleLogOut}
              className=" text-lg text-white px-4 py-1 rounded cursor-pointer "
            >
              <i className="fa fa-sign-in-alt mr-2" />
              Đăng Xuất
            </button>
          </div>
        </div>
      )
    } else {
      return (
        <div className="flex">
          <NavLink
            to="/login"
            className="hidden px-6 py-2 font-semibold rounded lg:block dark:bg-violet-400 dark:text-gray-900 text-white"
          >
            Sign in
          </NavLink>
          <NavLink
            to="/sign-up"
            className="hidden px-6 py-2 font-semibold rounded lg:block dark:bg-violet-400 dark:text-gray-900 text-white"
          >
            Sign up
          </NavLink>
        </div>
      )
    }
  }

  return (
    <header className="p-4 bg-black bg-opacity-80">
      <div className="container flex justify-between h-16 mx-auto sm:w-auto">
        <a
          href="/"
          aria-label="Back to homepage"
          className="flex items-center p-2"
        >
          <img
            src="https://cyberlearn.vn/wp-content/uploads/2020/03/cyberlearn-min-new-opt2.png"
            alt=""
          />
        </a>
        <CourseCatalog />
        <div className="flex items-center">
          <div className="relative">
            <span className="absolute inset-y-0 left-0 flex items-center pl-2">
              <button
                type="submit"
                title="Search"
                className="p-1 focus:outline-none focus:ring"
              >
                <svg
                  fill="currentColor"
                  viewBox="0 0 512 512"
                  className="w-4 h-4 dark:text-gray-100"
                >
                  <path d="M479.6,399.716l-81.084-81.084-62.368-25.767A175.014,175.014,0,0,0,368,192c0-97.047-78.953-176-176-176S16,94.953,16,192,94.953,368,192,368a175.034,175.034,0,0,0,101.619-32.377l25.7,62.2L400.4,478.911a56,56,0,1,0,79.2-79.195ZM48,192c0-79.4,64.6-144,144-144s144,64.6,144,144S271.4,336,192,336,48,271.4,48,192ZM456.971,456.284a24.028,24.028,0,0,1-33.942,0l-76.572-76.572-23.894-57.835L380.4,345.771l76.573,76.572A24.028,24.028,0,0,1,456.971,456.284Z" />
                </svg>
              </button>
            </span>
            <input
              type="search"
              name="Search"
              placeholder="Tìm khoá học..."
              className="w-32 py-2 pl-10 text-sm rounded-md sm:w-auto focus:outline-none dark:bg-gray-800 dark:text-gray-100 focus:dark:bg-gray-900"
            />
          </div>
          <div className="flex">{renderContent()}</div>
        </div>
        <button title="Open menu" type="button" className="p-4 lg:hidden">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            className="w-6 h-6 dark:text-gray-100"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M4 6h16M4 12h16M4 18h16"
            />
          </svg>
        </button>
      </div>
    </header>
  )
}
