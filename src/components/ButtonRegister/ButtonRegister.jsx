import React, { useEffect, useState } from 'react'
import { message } from 'antd'
import { userInforLocal } from 'services/local.service'
import { userServ } from 'services/usersServices'
import { coursesServices } from 'services/coursesServices'
import { useNavigate } from 'react-router'

export default function RenderButtonRegisterHome(props) {
  const navigate = useNavigate()
  const [currentUser, setCurrentUser] = useState([])
  let userInfor = userInforLocal.get()
  let taiKhoanLocal = userInfor?.taiKhoan
  let fetchUserInfor = () => {
    if (taiKhoanLocal) {
      userServ
        .getInforUser(taiKhoanLocal)
        .then((res) => {
          setCurrentUser(res.data)
        })
        .catch((err) => {
          message.error(err.response.data)
        })
    }
  }
  useEffect(() => {
    fetchUserInfor()
  }, [fetchUserInfor])
  let { maKhoaHoc } = props
  let dsKhoaHocGhiDanh = currentUser.chiTietKhoaHocGhiDanh
  const renderButtonRegisted = () => {
    let findIndex = (id, arr) => {
      for (let index = 0; index < arr?.length; index++) {
        var courseCurrent = arr[index]
        if (courseCurrent.maKhoaHoc === id) {
          return index
        }
      }
      return -1
    }
    let index = findIndex(maKhoaHoc, dsKhoaHocGhiDanh)
    return (
      <div>
        {index === -1 ? (
          <button
            onClick={handleRegisterCourse}
            className="font-bold text-white bg-green-500 hover:bg-slate-600 border-spacing-2 px-2 py-2 rounded-lg"
          >
            Đăng ký
          </button>
        ) : (
          <button
            onClick={handleCancelCourse}
            className="font-bold  text-white bg-red-500 hover:bg-slate-600 border-spacing-2 px-2 py-2 rounded-lg"
          >
            Hủy đăng ký
          </button>
        )}
      </div>
    )
  }

  const handleRegisterCourse = () => {
    if (userInfor) {
      let taiKhoanLocal = currentUser.taiKhoan
      let registerData = {
        maKhoaHoc: maKhoaHoc,
        taiKhoan: taiKhoanLocal,
      }

      coursesServices
        .postSignupCourse(registerData)
        .then((res) => {
          message.success('Hi! Bạn đã đăng ký khóa học thành công!')
          setTimeout(() => {
            window.location.reload()
          }, 1000)
        })
        .catch((err) => {
          message.error('Hi! Bạn ' + err.response.data)
          setTimeout(() => {
            window.location.reload()
          }, 1000)
        })
    } else {
      navigate('/login')
    }
  }
  const handleCancelCourse = () => {
    let taiKhoanLocal = currentUser.taiKhoan
    let registerData = {
      maKhoaHoc: maKhoaHoc,
      taiKhoan: taiKhoanLocal,
    }

    coursesServices
      .postCancelCourse(registerData)
      .then((res) => {
        message.success('Hi! Bạn đã HỦY đăng ký khóa học thành công!')
        setTimeout(() => {
          window.location.reload()
        }, 1000)
      })
      .catch((err) => {
        message.error('Hi! Bạn ' + err.response.data)
        setTimeout(() => {
          window.location.reload()
        }, 1000)
      })
  }
  return <div>{renderButtonRegisted()}</div>
}
