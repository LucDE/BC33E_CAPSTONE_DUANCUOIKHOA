import React, { useEffect, useState } from 'react'
import './DetailPage.css'
import { useParams } from 'react-router-dom'
import { coursesServices } from 'services/coursesServices'
import { Rate } from 'antd'
import { EyeOutlined } from '@ant-design/icons'
import RenderButtonRegister from 'components/ButtonRegister/ButtonRegister'

export default function DetailPage() {
  const [dataDetail, setDataDetail] = useState([])
  let params = useParams()
  let { id } = params

  const fetchData = async () => {
    try {
      const res = await coursesServices.getDetailCourse({ maKhoaHoc: id })
      setDataDetail(res.data)
    } catch (error) {}
  }
  useEffect(() => {
    fetchData()
  }, [])

  let {
    hinhAnh,
    danhMucKhoaHoc,
    luotXem,
    moTa,
    ngayTao,
    soLuongHocVien,
    maKhoaHoc,
  } = dataDetail
  return (
    <div>
      <div
        className="styleall"
        style={{
          backgroundImage: `url(${hinhAnh})`,
          height: '80vh',
          width: '100wh',
        }}
      >
        <div className={` style `}>
          <div className="grid grid-cols-12 mt-20">
            <div className="col-span-4 col-start-2">
              <div className="col-start-1">
                <img
                  style={{
                    width: '100%',
                    height: 200,
                    aspectRatio: 'auto',
                  }}
                  className="rounded-xl col-span-1 object-cover"
                  src={hinhAnh}
                  alt={hinhAnh}
                />
              </div>
            </div>
            <div className="col-span-5 col-start-7 text-white">
              <p className="text-6xl mb-2">
                {danhMucKhoaHoc?.tenDanhMucKhoaHoc}
              </p>
              <p className="text-xl">Ngày tạo : {ngayTao}</p>
              <p className="text-xl">
                Số lượng học viên hiện tại : {soLuongHocVien}{' '}
              </p>
              <p className="text-xl">
                Đánh giá khoá học : <Rate allowHalf defaultValue={4.5} /> 4.5|{' '}
                <EyeOutlined className="mb-5" /> ({luotXem})
              </p>
              <RenderButtonRegister maKhoaHoc={maKhoaHoc} />
            </div>
          </div>
        </div>
      </div>
      <div className="container font-bold p-5 bg-yellow-200">
        <p className="text-red-800 text-center text-4xl">MÔ TẢ KHOÁ HỌC</p>
        <div>
          <p className="text-xl pt-2">Mô tả khoá học : {moTa}</p>
        </div>
      </div>
    </div>
  )
}
