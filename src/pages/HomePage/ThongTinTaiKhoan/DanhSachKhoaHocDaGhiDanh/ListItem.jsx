import React from 'react'
import { NavLink } from 'react-router-dom'
import RenderButtonRegister from './ButtonRegister'

export default function ListItem(props) {
  let { tenKhoaHoc, maKhoaHoc } = props.item
  return (
    <div className=" px-3 p-4 h-300 ">
      <div className="h-100 bg-green-200 relative border-2 border-white-200 border-opacity-60 rounded-xl overflow-hidden ">
        <div className=" p-6 rounded-lg text-left   ">
          <h2 className="tracking-widest text-sm title-font font-medium text-green mb-1">
            Tên khoá Học
          </h2>
          <h1 className="title-font text-lg font-medium text-gray-900 mb-3">
            {tenKhoaHoc}
          </h1>
        </div>
        <NavLink
          to={`/detail/${maKhoaHoc}`}
          className=" text-red-500 absolute bottom-2 right-2 items-center "
        >
          <span className="px-1 mx-0">Chi tiết</span>
          <i className="fa-solid fa-square-up-right"></i>
        </NavLink>
        <RenderButtonRegister maKhoaHoc={maKhoaHoc} />
      </div>
    </div>
  )
}
