import { message } from 'antd'
import React, { useEffect, useState } from 'react'
import { userInforLocal } from 'services/local.service'
import { userServ } from 'services/usersServices'
import ListItem from './ListItem'
import styleSlick from './MultipleRowSlick.module.css'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import Slider from 'react-slick'

export default function KhoaHocDaGhiDanh() {
  const [currentUser, setCurrentUser] = useState([])
  let userInfor = userInforLocal.get()
  let taiKhoanLocal = userInfor.taiKhoan

  let fetchUserInfor = () => {
    userServ
      .getInforUser(taiKhoanLocal)
      .then((res) => {
        setCurrentUser(res.data)
      })
      .catch((err) => {
        message.error(err.response.data)
      })
  }

  useEffect(() => {
    fetchUserInfor()
  }, [])
  let { chiTietKhoaHocGhiDanh } = currentUser
  function SampleNextArrow(props) {
    const { className, style, onClick } = props
    return (
      <div
        className={`${className} ${styleSlick['slick-next']}`}
        style={{
          ...style,
          display: 'block',
          color: 'black',
          // backgroundColor: "green",
        }}
        onClick={onClick}
      ></div>
    )
  }

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props
    return (
      <div
        className={`${className} ${styleSlick['slick-prev']}`}
        style={{ ...style, display: 'block', left: '-50px' }}
        onClick={onClick}
      ></div>
    )
  }

  const settings = {
    className: 'center',
    centerMode: true,
    infinite: true,
    centerPadding: '10px',
    slidesToShow: 3,
    speed: 400,
    rows: 1,
    slidesPerRow: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  }
  return (
    // <div className="flex flex-wrap -m-4 rounded-2xl">
    <Slider {...settings} taiKhoan2={taiKhoanLocal}>
      {chiTietKhoaHocGhiDanh?.slice(0, 10).map((item, index) => {
        return <ListItem item={item} key={index} />
      })}
    </Slider>

    // </div>
  )
}
