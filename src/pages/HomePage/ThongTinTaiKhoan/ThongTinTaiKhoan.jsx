import React, { useEffect, useState } from 'react'
import { userInforLocal } from 'services/local.service'
import { userServ } from 'services/usersServices'
import { Button, Form, Input, message } from 'antd'
import KhoaHocDaGhiDanh from './DanhSachKhoaHocDaGhiDanh/KhoaHocDaGhiDanh'
import { useNavigate } from 'react-router'

export default function ThongTinTaiKhoan() {
  let navigate = useNavigate()
  const [currentUser, setCurrentUser] = useState([])
  let userInfor = userInforLocal.get()
  let taiKhoanLocal = userInfor.taiKhoan

  let fetchUserInfor = () => {
    userServ
      .getInforUser(taiKhoanLocal)
      .then((res) => {
        setCurrentUser(res.data)
      })
      .catch((err) => {
        message.error(err.response.data)
      })
  }

  useEffect(() => {
    fetchUserInfor()
  }, [])
  let { taiKhoan, matKhau, maLoaiNguoiDung, hoTen, email, soDT, maNhom } =
    currentUser
  const onFinish = (values) => {
    let { taiKhoan, matKhau, maLoaiNguoiDung, hoTen, email, soDT } = values

    let newData2 = {
      taiKhoan,
      matKhau,
      hoTen,
      soDT,
      maLoaiNguoiDung: currentUser.maLoaiNguoiDung,
      maNhom: currentUser.maNhom,
      email,
    }

    userServ
      .updateUserInfor(newData2)
      .then((res) => {
        message.success('Cập nhật thông tin người dùng thành công!')
        setTimeout(() => {
          userInforLocal.remove()
          window.location.reload(navigate('/login'))
        }, 1000)
      })
      .catch((err) => {
        message.error(err.response.data)
      })
  }
  let renderForm = () => {
    if (currentUser !== null) {
      return (
        <Form
          initialValues={{
            taiKhoan: currentUser?.taiKhoan,
            matKhau: currentUser?.matKhau,
            moTa: currentUser?.moTa,
            // moTa: currentUser?.moTa,
            hoTen: currentUser?.hoTen,
            maLoaiNguoiDung: currentUser?.maLoaiNguoiDung,
            email: currentUser?.email,
            soDT: currentUser?.soDT,
          }}
          id="form2"
          name="basic"
          labelCol={{
            span: 4,
          }}
          wrapperCol={{
            span: 14,
          }}
          layout="horizontal"
          onFinish={onFinish}
        >
          <Form.Item name="taiKhoan" label="Tài khoản">
            <Input
              className="mb-6 bg-gray-600 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-600 dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder={currentUser.taiKhoan}
            />
          </Form.Item>
          <Form.Item name="matKhau" label="Mật khẩu">
            <Input
              className="mb-6 bg-gray-600 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-600 dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder={currentUser.matKhau}
            />
          </Form.Item>
          <Form.Item name="hoTen" label="Họ tên">
            <Input
              className="mb-6 bg-gray-600 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-600 dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder={currentUser.hoTen}
            />
          </Form.Item>
          <Form.Item name="maLoaiNguoiDung" label="Loại người dùng">
            <Input
              placeholder={
                currentUser?.maLoaiNguoiDung === 'HV' ? 'Học viên' : 'Giáo vụ'
              }
              disabled={true}
            ></Input>
          </Form.Item>
          <Form.Item name="email" label="Email">
            <Input
              className="mb-6 bg-gray-600 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-600 dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder={currentUser.email}
            />
          </Form.Item>
          <Form.Item name="soDT" label="Số điện thoại">
            <Input
              className="mb-6 bg-gray-600 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-600 dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder={currentUser.soDT}
            />
          </Form.Item>
          <Form.Item label="Sửa thông tin">
            <Button
              className="text-center h-10 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
              htmlType="submit"
            >
              Xác nhận
            </Button>
          </Form.Item>
        </Form>
      )
    }
  }

  return (
    <div>
      <div className="container w-full flex flex-wrap mx-auto px-2 pt-8 lg:pt-16 mt-16">
        <div className="w-full lg:w-1/5 px-6 text-xl text-gray-800 leading-normal">
          <p className="text-base font-bold py-2 lg:pb-6 text-gray-700">
            Danh mục
          </p>
          <div className="block lg:hidden sticky inset-0">
            <button
              id="menu-toggle"
              className="flex w-full justify-end px-3 py-3 bg-white lg:bg-transparent border rounded border-gray-600 hover:border-yellow-600 appearance-none focus:outline-none"
            >
              <svg
                className="fill-current h-3 float-right"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
              </svg>
            </button>
          </div>
          <div
            className="w-full sticky inset-0 hidden lg:h-auto overflow-x-hidden overflow-y-auto lg:overflow-y-hidden lg:block mt-0 my-2 lg:my-0 border border-gray-400 lg:border-transparent bg-white shadow lg:shadow-none lg:bg-transparent z-20"
            style={{ top: '6em' }}
            id="menu-content"
          >
            <ul className="list-reset py-2 md:py-0 ">
              <li className="py-1 md:my-2 hover:bg-yellow-100 lg:hover:bg-transparent border-l-4 border-transparent font-bold border-yellow-600">
                <a
                  href="#section1"
                  className="block pl-4 align-middle text-gray-700 no-underline hover:text-yellow-600"
                >
                  <span className="pb-1 md:pb-0 text-sm">
                    Chi tiết thông tin cá nhân
                  </span>
                </a>
              </li>
              <li className="py-1 md:my-2 hover:bg-yellow-100 lg:hover:bg-transparent border-l-4 border-transparent">
                <a
                  href="#section2"
                  className="block pl-4 align-middle text-gray-700 no-underline hover:text-yellow-600"
                >
                  <span className="pb-1 md:pb-0 text-sm">
                    Cập nhật thông tin cá nhân
                  </span>
                </a>
              </li>
              <li className="py-1 md:my-2 hover:bg-yellow-100 lg:hover:bg-transparent border-l-4 border-transparent">
                <a
                  href="#section3"
                  className="block pl-4 align-middle text-gray-700 no-underline hover:text-yellow-600"
                >
                  <span className="pb-1 md:pb-0 text-sm">
                    Danh sách khóa học đã ghi danh
                  </span>
                </a>
              </li>
            </ul>
          </div>
        </div>
        {/*Section container*/}
        <section className="w-full lg:w-4/5">
          {/*Title*/}
          <h1 className="flex items-center font-sans font-bold break-normal text-gray-700 px-2 text-xl mt-12 lg:mt-0 md:text-2xl">
            Quản lý tài khoản cá nhân
          </h1>
          {/*divider*/}
          <hr className="bg-gray-300 my-12" />
          {/*Title*/}
          <h2
            id="section1"
            className="font-sans font-bold break-normal text-gray-700 px-2 pb-8 text-xl"
          >
            Chi tiết thông tin cá nhân
          </h2>
          {/*Card*/}
          <div className="p-8 mt-6 lg:mt-0 leading-normal rounded shadow bg-white">
            <form className="w-full max-w-lg">
              <div className="flex flex-wrap -mx-3 mb-6">
                <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                  <label
                    className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                    htmlFor="grid-first-name"
                  >
                    Họ và tên
                  </label>
                  <input
                    type="text"
                    id="disabled-input"
                    aria-label="disabled input"
                    class="mb-6 bg-gray-600 border border-gray-300 text-gray-900 text-sm rounded-lg cursor-not-allowed focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-600 dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    value={hoTen}
                    disabled
                  ></input>
                </div>
                <div className="w-full md:w-1/2 px-3">
                  <label
                    className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                    htmlFor="grid-last-name"
                  >
                    E-mail
                  </label>
                  <input
                    type="text"
                    id="disabled-input"
                    aria-label="disabled input"
                    class="mb-6 bg-gray-600 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 cursor-not-allowed dark:bg-gray-600 dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    value={email}
                    disabled
                  ></input>
                </div>
              </div>
              <div className="flex flex-wrap -mx-3 mb-6">
                <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                  <label
                    className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                    htmlFor="grid-first-name"
                  >
                    Tài khoản
                  </label>
                  <input
                    type="text"
                    id="disabled-input"
                    aria-label="disabled input"
                    class="mb-6 bg-gray-600 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 cursor-not-allowed dark:bg-gray-600 dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    value={taiKhoan}
                    disabled
                  ></input>
                </div>
                <div className="w-full md:w-1/2 px-3">
                  <label
                    className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                    htmlFor="grid-last-name"
                  >
                    Mật Khẩu
                  </label>
                  <input
                    type="password"
                    id="disabled-input"
                    aria-label="disabled input"
                    class="mb-6 bg-gray-600 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 cursor-not-allowed dark:bg-gray-600 dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    value={matKhau}
                    disabled
                  ></input>
                </div>
              </div>

              <div className="flex flex-wrap -mx-3 mb-2">
                <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                  <label
                    className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                    htmlFor="grid-city"
                  >
                    Mã nhóm
                  </label>
                  <input
                    type="text"
                    id="disabled-input"
                    aria-label="disabled input"
                    class="mb-6 bg-gray-600 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 cursor-not-allowed dark:bg-gray-600 dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    value={maNhom}
                    disabled
                  ></input>
                </div>
                <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                  <label
                    className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                    htmlFor="grid-state"
                  >
                    Loại người dùng
                  </label>
                  <input
                    type="text"
                    id="disabled-input"
                    aria-label="disabled input"
                    class="mb-6 bg-gray-600 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 cursor-not-allowed dark:bg-gray-600 dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    value={maLoaiNguoiDung === 'HV' ? 'Học viên' : 'Giáo vụ'}
                    disabled
                  ></input>
                </div>
                <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                  <label
                    className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                    htmlFor="grid-state"
                  >
                    Số điện thoại
                  </label>
                  <input
                    type="text"
                    id="disabled-input"
                    aria-label="disabled input"
                    class="mb-6 bg-gray-600 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 cursor-not-allowed dark:bg-gray-600 dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    value={soDT}
                    disabled
                  ></input>
                </div>
                <div className="relative w-full md:w-1/3 px-3 mb-6 md:mb-0 ">
                  <a
                    href="#section2"
                    className="text-center h-10 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                  >
                    <span className="pb-1 md:pb-0 text-sm">Cập nhật</span>
                  </a>
                </div>
              </div>
            </form>
          </div>
          {/*/Card*/}
          {/*divider*/}
          <hr className="bg-gray-300 my-12" />
          {/*Title*/}
          <h2 className="font-sans font-bold break-normal text-gray-700 px-2 pb-8 text-xl">
            Cập nhật thông tin cá nhân
          </h2>
          {/*Card*/}
          <div
            id="section2"
            className="p-8 mt-6 lg:mt-0 rounded shadow bg-white"
          >
            {renderForm()}
          </div>
          {/*/Card*/}
          {/*divider*/}
          <hr className="bg-gray-300 my-12" />
          {/*Title*/}
          <h2 className="font-sans font-bold break-normal text-gray-700 px-2 pb-8 text-xl">
            Danh sách khóa học đã ghi danh
          </h2>
          {/*Card*/}
          <div
            id="section3"
            className="p-8 mt-6 h-auto lg:mt-0 rounded shadow bg-white"
          >
            <KhoaHocDaGhiDanh />
          </div>
        </section>
        {/*/Section container*/}
        {/*Back link */}
        <div className="w-full lg:w-4/5 lg:ml-auto text-base md:text-sm text-gray-600 px-4 py-24 mb-12">
          <span className="text-base text-yellow-600 font-bold">&lt;</span>
          <a
            href="#"
            className="text-base md:text-sm text-yellow-600 font-bold no-underline hover:underline"
          >
            Trở về đầu trang
          </a>
        </div>
      </div>
    </div>
  )
}
