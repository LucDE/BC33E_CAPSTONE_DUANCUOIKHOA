import React, { useState, useEffect } from 'react'
import { userServ } from 'services/usersServices'
import { useNavigate, useParams } from 'react-router-dom'
import { Button, Form, Input, message, Select } from 'antd'
const { Option } = Select

export default function EditUserPage() {
  const [user, setUser] = useState(null)

  let navigate = useNavigate()

  let params = useParams()

  let { taiKhoan } = params
  console.log('taiKhoan: ', taiKhoan)

  let fetchUserInfor = () => {
    userServ
      .getUserForEditPage(taiKhoan)
      .then((res) => {
        console.log('res: ', res)
        setUser(res.data)
      })
      .catch((err) => {
        message.error(err.response.data)
      })
  }
  useEffect(() => {
    fetchUserInfor()
  }, [])

  const onFinish = (values) => {
    let { taiKhoan, matKhau, maLoaiNguoiDung, hoTen, email, soDT } = values

    let newData = {
      taiKhoan,
      matKhau,
      hoTen,
      soDT,
      maLoaiNguoiDung,
      maNhom: user.maNhom,
      email,
    }

    userServ
      .updateUserInfor(newData)
      .then((res) => {
        message.success('Cập nhật thông tin người dùng thành công!')
        setTimeout(() => {
          navigate('/admin')
        }, 1000)
      })
      .catch((err) => {
        message.error(err.response.data)
      })
  }

  let renderForm = () => {
    if (user !== null) {
      return (
        <Form
          initialValues={{
            taiKhoan: user?.taiKhoan,
            matKhau: user?.matKhau,
            moTa: user?.moTa,
            hoTen: user?.hoTen,
            maLoaiNguoiDung: user?.maLoaiNguoiDung,
            email: user?.email,
            soDT: user?.soDT,
          }}
          id="form"
          name="basic"
          labelCol={{
            span: 4,
          }}
          wrapperCol={{
            span: 14,
          }}
          layout="horizontal"
          onFinish={onFinish}
        >
          <Form.Item name="taiKhoan" label="Tài khoản">
            <Input />
          </Form.Item>
          <Form.Item name="matKhau" label="Mật khẩu">
            <Input />
          </Form.Item>
          <Form.Item name="hoTen" label="Họ tên">
            <Input />
          </Form.Item>
          <Form.Item
            name="maLoaiNguoiDung"
            label="Loại người dùng"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select placeholder="Chọn loại người dùng" allowClear>
              <Option value="HV">Học viên</Option>
              <Option value="GV">Giáo vụ</Option>
            </Select>
          </Form.Item>
          <Form.Item name="email" label="Email">
            <Input />
          </Form.Item>
          <Form.Item name="soDT" label="Số điện thoại">
            <Input />
          </Form.Item>
          <Form.Item label="Sửa thông tin">
            <Button htmlType="submit">Xác nhận</Button>
          </Form.Item>
        </Form>
      )
    }
  }

  return (
    <div className="container  px-10 py-32">
      <div className="border-4 border-red-500 py-5 bg-slate-500">
        <h1 className="text-3xl text-center font-bold mb-5">EDIT USER</h1>
        {renderForm()}
      </div>
    </div>
  )
}
