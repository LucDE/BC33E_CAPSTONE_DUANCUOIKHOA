import { Button } from 'antd'

export const createHeaderUsersTable1 = (onRegister, onDelete) => {
  return [
    {
      title: 'STT',
      dataIndex: 'key',
      key: 'stt',
    },
    {
      title: 'Họ tên',
      dataIndex: 'hoTen',
      key: 'hoTen',
    },
    {
      title: 'Tài khoản',
      dataIndex: 'taiKhoan',
      key: 'taiKhoan',
    },
    {
      title: 'Bí danh',
      dataIndex: 'biDanh',
      key: 'biDanh',
    },
    {
      title: 'Thao tác',
      key: 'action',
      render: ({ taiKhoan }) => {
        return (
          <div className="space-x-4">
            <Button
              className="bg-amber-500 text-white rounded"
              onClick={() => {
                onRegister(taiKhoan)
              }}
            >
              Xác thực ghi danh
            </Button>
            <Button
              className="bg-red-500 text-white  rounded"
              onClick={() => {
                onDelete(taiKhoan)
              }}
            >
              Hủy ghi danh
            </Button>
          </div>
        )
      },
    },
  ]
}
