import { Button } from 'antd'
import { NavLink } from 'react-router-dom'

export const createHeaderCoursesTable = (onDelete) => {
  return [
    {
      title: 'STT',
      dataIndex: 'key',
      key: 'stt',
    },
    {
      title: 'Mã khóa học',
      dataIndex: 'maKhoaHoc',
      key: 'maKhoaHoc',
    },
    {
      title: 'Tên khóa học',
      dataIndex: 'tenKhoaHoc',
      key: 'tenKhoaHoc',
    },
    {
      title: 'Hình ảnh',
      key: 'hinhAnh',
      render: ({ hinhAnh }) => {
        return <img width="100" alt={hinhAnh} src={hinhAnh}></img>
      },
    },
    {
      title: 'Lượt xem',
      dataIndex: 'luotXem',
      key: 'luotXem',
    },
    {
      title: 'Mô tả',
      key: 'moTa',
      render: ({ moTa }) => {
        return (
          <span>
            {moTa
              ? moTa.substring(0, 50) + '...'
              : 'Chưa có mô tả cho khóa học này'}
          </span>
        )
      },
    },
    {
      title: 'Thao tác',
      key: 'action',
      render: ({ maKhoaHoc }) => {
        return (
          <div className="space-x-4">
            <Button className="bg-amber-500 text-white rounded">
              <NavLink to={`/register-user/${maKhoaHoc}`}>Ghi danh</NavLink>
            </Button>
            <Button className="bg-blue-500 text-white rounded">
              <NavLink to={`/edit-course/${maKhoaHoc}`}>Sửa</NavLink>
            </Button>
            <Button
              className="bg-red-500 text-white  rounded"
              onClick={() => {
                onDelete(maKhoaHoc)
              }}
            >
              Xóa
            </Button>
          </div>
        )
      },
    },
  ]
}
