import { Tabs } from 'antd'
import React from 'react'
import CourseManament from './CourseManagement'
import UserManagement from './UserManagement'

export default function ManagementPage() {
  return (
    <div className="container  px-10 py-10">
      <div className="border-2 border-black p-3">
        <Tabs tabPosition="top">
          <Tabs.TabPane
            tab={<p className="font-bold">Quản lý người dùng</p>}
            key="item-tab-1"
          >
            <UserManagement />
          </Tabs.TabPane>
          <Tabs.TabPane
            tab={<p className="font-bold">Quản lý khóa học</p>}
            key="item-tab-2"
          >
            <CourseManament />
          </Tabs.TabPane>
        </Tabs>
      </div>
    </div>
  )
}
