import { Tag, Button } from 'antd'
import { NavLink } from 'react-router-dom'

export const createHeaderUsersTable = (onDelete) => {
  return [
    {
      title: 'STT',
      dataIndex: 'key',
      key: 'stt',
    },
    {
      title: 'Họ tên',
      dataIndex: 'hoTen',
      key: 'hoTen',
    },
    {
      title: 'Tài khoản',
      dataIndex: 'taiKhoan',
      key: 'taiKhoan',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },

    {
      title: 'Số điện thoại',
      dataIndex: 'soDt',
      key: 'soDt',
    },
    {
      title: 'Loại người dùng',
      dataIndex: 'maLoaiNguoiDung',
      key: 'maLoaiNguoiDung',
      render: (text) => {
        let color = text === 'GV' ? 'blue' : 'red'
        let newName = text === 'GV' ? 'Giáo vụ' : 'Học viên'
        return <Tag color={color}>{newName}</Tag>
      },
    },
    {
      title: 'Thao tác',
      key: 'action',
      render: ({ taiKhoan }) => {
        return (
          <div className="space-x-4">
            <Button className="bg-amber-500 text-white rounded">
              <NavLink to={`/register-course/${taiKhoan}`}>Ghi danh</NavLink>
            </Button>
            <Button className="bg-blue-500 text-white rounded">
              <NavLink to={`/edit-user/${taiKhoan}`}>Sửa</NavLink>
            </Button>
            <Button
              className="bg-red-500 text-white  rounded"
              onClick={() => {
                onDelete(taiKhoan)
              }}
            >
              Xóa
            </Button>
          </div>
        )
      },
    },
  ]
}
