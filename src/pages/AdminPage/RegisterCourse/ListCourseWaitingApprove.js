import React, { useEffect, useState } from 'react'
import _ from 'lodash'
import { useParams } from 'react-router-dom'
import { message } from 'antd'
import { userServ } from 'services/usersServices'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import ListCourseWaiting from './ListCourseWaiting'

export default function ListCourseWaitingApprove() {
  const [listCourseWaiting, setCourseWaiting] = useState(null)
  const params = useParams()
  let { taiKhoan } = params

  let fetchCourseInfor = () => {
    userServ
      .getCourseWaitingApprove(taiKhoan)
      .then((res) => {
        setCourseWaiting(res.data)
      })

      .catch((err) => {
        message.error(err.response.data)
      })
  }

  useEffect(() => {
    fetchCourseInfor()
  }, [])
  return (
    <div className="grid grid-cols-4 gap-4">
      {listCourseWaiting?.map((course, index) => {
        return <ListCourseWaiting key={index} item={course} />
      })}
    </div>
  )
}
