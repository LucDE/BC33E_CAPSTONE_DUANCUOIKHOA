import React, { useEffect, useState } from 'react'
import _ from 'lodash'
import { useParams } from 'react-router-dom'
import { message } from 'antd'
import { userServ } from 'services/usersServices'
import { nanoid } from 'nanoid'

import Slider from 'react-slick'
import styleSlick from './MultipleRowSlick.module.css'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import ListCourse from './ListCourse'

export default function ListCourseNotRegisted() {
  const [listNotRegisted, setCourseListNotRegisted] = useState(null)
  const params = useParams()
  let { taiKhoan } = params

  let fetchCourseInfor = () => {
    userServ
      .getCourseNotRegisted(taiKhoan)
      .then((res) => {
        setCourseListNotRegisted(res.data)
      })

      .catch((err) => {
        message.error(err.response.data)
      })
  }

  useEffect(() => {
    fetchCourseInfor()
  }, [])

  function SampleNextArrow(props) {
    const { className, style, onClick } = props
    return (
      <div
        className={`${className} ${styleSlick['slick-next']}`}
        style={{
          ...style,
          display: 'block',
          color: 'black',
          // backgroundColor: "green",
        }}
        onClick={onClick}
      ></div>
    )
  }

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props
    return (
      <div
        className={`${className} ${styleSlick['slick-prev']}`}
        style={{ ...style, display: 'block', left: '-50px' }}
        onClick={onClick}
      ></div>
    )
  }

  const settings = {
    className: 'center',
    centerMode: false,
    infinite: true,
    centerPadding: '10px',
    slidesToShow: 4,
    speed: 400,
    rows: 2,
    slidesPerRow: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  }
  return (
    <div>
      <Slider {...settings} taiKhoan2={taiKhoan}>
        {listNotRegisted?.map((course) => {
          return <ListCourse key={nanoid()} item={course} />
        })}
      </Slider>
    </div>
  )
}
