import React from 'react'
import { Button, Form, Input, message } from 'antd'
import { useNavigate } from 'react-router-dom'
import { userServ } from 'services/usersServices'
import {
  LockOutlined,
  UserOutlined,
  MailOutlined,
  ProfileOutlined,
  ToolOutlined,
  PhoneOutlined,
} from '@ant-design/icons'

import './index.scss'

const styleSignUpPage = {
  width: '100vw',
  height: '100vh',
  backgroundImage:
    'linear-gradient(to top,#000,transparent 100%),url(https://png.pngtree.com/thumb_back/fh260/background/20220427/pngtree-webinar-banner-e-learning-internet-lesson-image_1091839.jpg)',
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
}
export default function SignUpPage() {
  let navigate = useNavigate()

  const onFinish = (values) => {
    let { taiKhoan, matKhau, email, soDt, hoTen } = values
    let newAccount = {
      taiKhoan,
      matKhau,
      hoTen,
      soDt,
      maNhom: 'GP01',
      email,
    }

    userServ
      .postSignUp(newAccount)
      .then((res) => {
        message.success('Đăng ký thành công')
        setTimeout(() => {
          navigate('/login')
        }, 1000)
      })
      .catch((err) => {
        message.error(err.response.data)
      })
  }
  const onFinishFailed = (errorInfo) => {}

  return (
    <div
      style={styleSignUpPage}
      className="h-screen w-screen flex justify-center items-center p-5"
      id="signup__page"
    >
      <div className="container mx-auto p-5  rounded-lg flex justify-center">
        <div className="df:w-full md:w-2/3 lg:w-2/5 bg-slate-200 p-5 rounded-xl ">
          <div className="flex flex-col items-center mb-5">
            <ToolOutlined className="text-4xl text-red-500" />
            <h4 className="text-xl font-bold">Đăng ký</h4>
          </div>
          <Form
            layout="vertical"
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: 'Trường này yêu cầu nhập!',
                },
              ]}
            >
              <Input
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="Tài khoản*"
              />
            </Form.Item>

            <Form.Item
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: 'Trường này yêu cầu nhập!',
                },
              ]}
            >
              <Input.Password
                prefix={<LockOutlined className="site-form-item-icon" />}
                placeholder="Mật khẩu*"
              />
            </Form.Item>
            <Form.Item
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: 'Trường này yêu cầu nhập!',
                },
              ]}
            >
              <Input.Password
                prefix={<LockOutlined className="site-form-item-icon" />}
                placeholder="Nhập lại mật khẩu*"
              />
            </Form.Item>
            <Form.Item
              name="hoTen"
              rules={[
                {
                  required: true,
                  message: 'Trường này yêu cầu nhập!',
                },
              ]}
            >
              <Input
                prefix={<ProfileOutlined className="site-form-item-icon" />}
                placeholder="Họ tên*"
              />
            </Form.Item>

            <Form.Item
              name="email"
              rules={[
                {
                  required: true,
                  message: 'Trường này yêu cầu nhập!',
                },
              ]}
            >
              <Input
                prefix={<MailOutlined className="site-form-item-icon" />}
                placeholder="Email*"
              />
            </Form.Item>
            <Form.Item
              name="soDt"
              rules={[
                {
                  required: true,
                  message: 'Trường này yêu cầu nhập!',
                },
              ]}
            >
              <Input
                prefix={<PhoneOutlined className="site-form-item-icon" />}
                placeholder="Số điện thoại*"
              />
            </Form.Item>
            <div className="flex justify-center">
              <Form.Item>
                <Button
                  className="bg-red-500 text-white rounded"
                  htmlType="submit"
                >
                  Đăng ký
                </Button>
              </Form.Item>
            </div>
          </Form>
        </div>
      </div>
    </div>
  )
}
