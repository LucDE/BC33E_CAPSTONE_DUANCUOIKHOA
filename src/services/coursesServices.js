import axios from 'axios'
import { userInforLocal } from './local.service'
import { BASE_URL, https, TOKEN_CYBERSOFT } from './url.config'

export const coursesServices = {
  getCourseList: (params) => {
    let uri = '/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc'
    return https.get(uri, { params })
  },
  getCourseCatalog: () => {
    let uri = '/api/QuanLyKhoaHoc/LayDanhMucKhoaHoc'
    return https.get(uri)
  },
  getCoursesByCategory: (params) => {
    let uri = '/api/QuanLyKhoaHoc/LayKhoaHocTheoDanhMuc'
    return https.get(uri, { params })
  },
  addCourse: (data) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyKhoaHoc/ThemKhoaHocUploadHinh`,
      method: 'POST',
      headers: {
        'Access-Control-Allow-Origin': '*',
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
      },
      data,
    })
  },
  getDetailCourse: (params) => {
    let uri = '/api/QuanLyKhoaHoc/LayThongTinKhoaHoc'
    return https.get(uri, { params })
  },
  getDetailCourseAdmin: (MaKhoaHoc) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyKhoaHoc/LayThongTinKhoaHoc?maKhoaHoc=${MaKhoaHoc}`,
      method: 'GET',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
      },
    })
  },
  deleteCourse: (MaKhoaHoc) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyKhoaHoc/XoaKhoaHoc?maKhoaHoc=${MaKhoaHoc}`,
      method: 'DELETE',
      headers: {
        'Access-Control-Allow-Origin': '*',
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
      },
    })
  },
  updateCourse: (data) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyKhoaHoc/CapNhatKhoaHoc`,
      method: 'PUT',
      headers: {
        'Access-Control-Allow-Origin': '*',
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
      },
      data,
    })
  },
  uploadImage: (data) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyKhoaHoc/UploadHinhAnhKhoaHoc`,
      method: 'POST',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
      },
      data,
    })
  },
  postSignupCourse: (data) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyKhoaHoc/DangKyKhoaHoc`,
      method: 'POST',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
      },
      data,
    })
  },
  postCancelCourse: (data) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyKhoaHoc/HuyGhiDanh`,
      method: 'POST',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
      },
      data,
    })
  },
  getUserNotRegisterAdmin: (maKhoaHoc) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/LayDanhSachNguoiDungChuaGhiDanh`,
      method: 'POST',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
      },
      data: {
        MaKhoaHoc: maKhoaHoc,
      },
    })
  },
  getUserCoursePendingAdmin: (maKhoaHoc) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/LayDanhSachHocVienChoXetDuyet`,
      method: 'POST',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
      },
      data: {
        MaKhoaHoc: maKhoaHoc,
      },
    })
  },
  getUserCourseAttendAdmin: (maKhoaHoc) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/LayDanhSachHocVienKhoaHoc`,
      method: 'POST',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
      },
      data: {
        MaKhoaHoc: maKhoaHoc,
      },
    })
  },
  authenticateUserAttendCourseAdmin: (maKhoaHoc, taiKhoan) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyKhoaHoc/GhiDanhKhoaHoc`,
      method: 'POST',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
      },
      data: {
        maKhoaHoc: maKhoaHoc,
        taiKhoan: taiKhoan,
      },
    })
  },
  approveCourseRegister: (data) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyKhoaHoc/GhiDanhKhoaHoc`,
      method: 'POST',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
      },
      data,
    })
  },
}
