/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      screens: {
        df: '0px',
      },
      width: {
        800: '800px',
      },
      height: {
        300: '300px',
        400: '400px',
        500: '500px',
        700: '700px',
      },
      fontSize: {
        small: [
          '0.6rem',
          {
            lineHeight: '0.8rem',
            letterSpacing: '-0.01em',
            fontWeight: '500',
          },
        ],
      },
    },
  },
  plugins: [],
}
